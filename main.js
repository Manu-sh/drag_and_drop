function dragAndDrop(from, to) {

	dragAndDrop.swapSelectContent = function (n0, n1) {
		const tmp = n0.val();
		n0.val(n1.val()).trigger('change');
		n1.val(tmp).trigger('change');
	};

	dragAndDrop.swapInputText = function (n0, n1) {
		const tmp = n0.val();
		n0.val(n1.val()).trigger('change');
		n1.val(tmp).trigger('change');
	};

	dragAndDrop.swap = function (unused, i, j) {
		dragAndDrop.swapSelectContent($(`#merchant_${i}`), $(`#merchant_${j}`));
		dragAndDrop.swapInputText($(`#caption_${i}`), $(`#caption_${j}`));
	};

	dragAndDrop.rmove = function (a, from, to) {
		for (; from < to; ++from)
			dragAndDrop.swap(a, from, from + 1);
		return a;
	};

	dragAndDrop.lmove = function (a, from, to) {
		for (; from > to; --from) {
			dragAndDrop.swap(a, from, from - 1);
		}
		return a;
	};

	return from > to ? dragAndDrop.lmove(undefined, from, to) : dragAndDrop.rmove(undefined, from, to);
}

window.onload = (function () {

	$('.draggable').draggable({
		revert: true,
		revertDuration: false,
		zIndex: 1,
		snap: '.draggable',
		snapMode: 'inner',
		snapTolerance: 40,
		start: function () {
			$(this).css('cursor', 'grabbing');
		},
		stop: function () {
			$(this).css('cursor', 'grab');
		}
	}).css('cursor', 'grab');

	$('.droppable').droppable({
		accept: '.draggable, .droppable',
		tolerance: 'intersect',
		drop: function (event, ui) {
			let to = $(this).find('select').attr('id');
			let from = $(ui.draggable).find('select').attr('id');

			from = Number(from.substr(from.indexOf('_') + 1));
			to = Number(to.substr(to.indexOf('_') + 1));
			dragAndDrop(from, to);
		}
	});

});